package aula;

public class Conta {
	
	private int agencia;
	private int conta;
	private int dac;
	private double saldo;
	private String tipo;
	
	public Conta(int agencia, int conta, int dac, String tipo) {
		this.agencia = agencia;
		this.conta   = conta;
		this.dac     = dac;
		this.tipo    = tipo;
		saldo = 0;
	}

	boolean sacar(double valor) {
		if(saldo >= valor) {
			saldo -= valor;
			return true;
		}
		return false;
	}
	
	public void depositar(double valor) {
		saldo += valor;
	}
	
	public double getSaldo() {
		return saldo;
	}
	
	public String toString() {
		return agencia + "|" + conta + "-" + dac;
	}
	
}
