package aula;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Crie um programa que lê os clientes presentes no arquivo contas.csv e filtre
 * aqueles que possuem o saldo superior à 7000.
 * 
 * Desses clientes, criar outro arquivo de texto que possua todos os clientes no
 * seguinte formato:
 * 
 * João da Silva<joaosilva@teste.com>, Maria da Penha<maria@teste.com>
 * 
 */

public class SaqueConta {

	public static void main(String[] args) {

		// leitura de arquivo
		Path pathEntrada = Paths.get("exemplo.txt");

		String linhaSaida;

		int contador = 0;
		int contador2 = 0;

		StringBuilder builderSaida = new StringBuilder();

		try {
			List<String> listaContas = Files.readAllLines(pathEntrada);
			for (String line : listaContas) {
				contador++;
				if (contador > 1) {
					String array[] = new String[4];
					array = line.split(",");
					if (Integer.parseInt(array[4]) >= 7000) {
						linhaSaida = array[1] + " " + array[2] + "<" + array[3] + ">, ";
						builderSaida.append(linhaSaida);
						contador2++;

					}
				}
			}
			System.out.println(contador2);
			Path pathSaida = Paths.get("saida.txt");
			Files.write(pathSaida, builderSaida.toString().getBytes());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
